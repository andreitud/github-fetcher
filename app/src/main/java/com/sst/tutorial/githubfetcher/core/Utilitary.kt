package com.sst.tutorial.githubfetcher.core

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("photoUrl")
fun ImageView.photoUrl(imageUrl: String?) {
    Glide.with(this)
        .load(imageUrl)
        .into(this)
}

@BindingAdapter("customTextPrefix", "customTextSuffix")
fun customText(textView: TextView, prefix: String?, number: Int?) {
    if(prefix == null || number == null) return
    with(textView) {
        text = ""
        append(prefix)
        append(": ")
        append(number.toString())
    }
}
