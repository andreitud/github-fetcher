package com.sst.tutorial.githubfetcher.ui.list.repo

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sst.tutorial.githubfetcher.core.database.GithubDatabase
import com.sst.tutorial.githubfetcher.core.database.entities.RepoEntity
import com.sst.tutorial.githubfetcher.core.repository.GithubRepository
import com.sst.tutorial.githubfetcher.core.service.GithubService
import kotlinx.coroutines.*
import java.lang.IllegalArgumentException

class RepoListViewModel(private val repository: GithubRepository, private val userLogin: String?): ViewModel() {
    val repoList = MutableLiveData<ArrayList<RepoEntity>>(null)
    val textStatus = Transformations.map(repoList) {
        if(it == null) {
            "Loading..."
        }
        else {
            "Data loaded."
        }
    }

    private val job = Job()
    private val viewModeScope = CoroutineScope(Dispatchers.IO + job)

    fun fetchData() {
        viewModeScope.launch {
            withContext(Dispatchers.Default) {
                val repoEntityList = repository.getUserRepos(userLogin)
                repoList.postValue(repoEntityList)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }

    class Factory(private val context: Context, private val userLogin: String?): ViewModelProvider.Factory {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if(modelClass.isAssignableFrom(RepoListViewModel::class.java)) {
                val database = GithubDatabase(context)
                val repository = GithubRepository(GithubService(), database)
                return RepoListViewModel(repository, userLogin) as T
            }
            throw IllegalArgumentException("Class not assignable from RepoListViewModel in " +
                    "RepoListViewModel.Factory#create method.")
        }

    }
}