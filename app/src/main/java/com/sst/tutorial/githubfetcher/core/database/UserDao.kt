package com.sst.tutorial.githubfetcher.core.database

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.sst.tutorial.githubfetcher.core.database.entities.FollowCrossRef
import com.sst.tutorial.githubfetcher.core.database.entities.ProfileEntity
import com.sst.tutorial.githubfetcher.core.database.entities.UserEntity
import com.sst.tutorial.githubfetcher.core.database.entities.UserWithRepos

@Dao
interface UserDao: BaseDao<UserEntity> {
    @Transaction
    @Query("UPDATE github_user SET logged_in = 1 WHERE lower(login) like lower(:username)")
    suspend fun loginUser(username: String)

    @Transaction
    @Query("UPDATE github_user SET logged_in = 0")
    suspend fun logoutUser()

    @Query("SELECT * FROM github_user WHERE logged_in = 1")
    suspend fun getLoggedProfile(): ProfileEntity

    @Query("SELECT * FROM github_user WHERE logged_in = 1")
    suspend fun getLoggedUser(): UserEntity

    @Query("SELECT count(*) FROM github_user WHERE logged_in = 1")
    suspend fun userLoggedIn(): Int

    @Query("SELECT * FROM github_user WHERE user_id in (:userIdList)")
    suspend fun getUsersInList(userIdList: List<Long>): List<UserEntity>

    @Query("SELECT * FROM github_user WHERE user_id = :userId")
    suspend fun getUser(userId: Long): UserEntity?

    @Transaction
    @Query("SELECT * FROM github_user WHERE user_id = :userId")
    suspend fun getProfile(userId: Long): ProfileEntity?

    @Transaction
    @Query("SELECT * FROM github_user WHERE lower(login) = lower(:userLogin)")
    suspend fun getProfile(userLogin: String): ProfileEntity?

    @Transaction
    @Query("SELECT * FROM github_user WHERE user_id = :userId")
    suspend fun getUserWithRepos(userId: Long): UserWithRepos?

    @Query("DELETE FROM github_user")
    suspend fun clear()

    @Transaction
    @Query("SELECT * FROM github_user WHERE user_id = :userId")
    suspend fun getFollowings(userId: Long): FollowCrossRef.FollowingOfUser

    @Transaction
    @Query("SELECT * FROM github_user WHERE user_id = :userId")
    suspend fun getFollowers(userId: Long): FollowCrossRef.FollowersOfUser

    @Transaction
    suspend fun insertOrReplace(user: UserEntity) {
        val exists = update(user)
        if(exists == 0) {
            insert(user)
        }
    }

    @Transaction
    suspend fun insertOrReplaceArray(users: ArrayList<UserEntity>) {
        val updated = updateArray(users)
        if(updated < users.size) {
            insertArray(users)
        }
    }

}