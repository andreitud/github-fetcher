package com.sst.tutorial.githubfetcher.core.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "github_user_info")
data class UserInfoEntity(
    @PrimaryKey @ColumnInfo(name = "user_info_id")
    val userInfoId: Long = 0L,
    val name: String? = null,
    val company: String? = null,
    val location: String? = null,
    val email: String? = null,
    val bio: String? = null,
    @ColumnInfo(name = "public_repos") val publicRepos: Int = 0,
    @ColumnInfo(name = "public_gists") val publicGists: Int = 0,
    val followers: Int = 0,
    val following: Int = 0,
    @ColumnInfo(name = "created_at") val createdAt: String = "",
    @ColumnInfo(name = "updated_at") val updatedAt: String = ""
) {

}