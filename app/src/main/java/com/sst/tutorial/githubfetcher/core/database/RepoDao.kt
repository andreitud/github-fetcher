package com.sst.tutorial.githubfetcher.core.database

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.sst.tutorial.githubfetcher.core.database.entities.RepoEntity

@Dao
interface RepoDao: BaseDao<RepoEntity> {
    @Query("SELECT * FROM github_repo")
    suspend fun getAllRepos(): List<RepoEntity>

    @Query("SELECT * FROM github_repo WHERE owner_id = :userId")
    suspend fun getReposOfUser(userId: Long): List<RepoEntity>

    @Transaction
    @Query("""
        SELECT * FROM github_repo 
        JOIN github_user ON owner_id = user_id 
        WHERE lower(login) == lower(:userLogin)""")
    suspend fun getReposOfUser(userLogin: String): List<RepoEntity>

    @Query("SELECT * FROM github_repo WHERE repo_id = :repoId")
    suspend fun getRepo(repoId: Long): RepoEntity?

    @Transaction
    @Update
    suspend fun updateRepos(repos: ArrayList<RepoEntity>) {
        for (repo in repos) {
            update(repo)
        }
    }

    @Query("DELETE FROM github_repo")
    suspend fun clear()
}