package com.sst.tutorial.githubfetcher.core.database

import androidx.room.*
import com.sst.tutorial.githubfetcher.core.database.entities.FollowCrossRef
import com.sst.tutorial.githubfetcher.core.database.entities.UserEntity

@Dao
interface FollowDao: BaseDao<FollowCrossRef> {
    @Query("SELECT * FROM github_follow_relation WHERE followed_user = :userId")
    suspend fun getFollowersOf(userId: Long): List<FollowCrossRef>

    @Query("SELECT * FROM github_follow_relation WHERE following_user = :userId")
    suspend fun getFollowingsOf(userId: Long): List<FollowCrossRef>

    @Transaction
    @Insert
    suspend fun insertFollow(userFollowing: UserEntity, userFollowed: UserEntity) {
        val followCrossRef = FollowCrossRef(followingUser = userFollowing.userId, followedUser = userFollowed.userId)
        insert(followCrossRef)
    }

    @Transaction
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun bulkInsertFollowersOfUser(userFollowed: UserEntity, followers: List<UserEntity>) {
        val followCrossRefList = followers.map { FollowCrossRef(it.userId, userFollowed.userId) }
        val followArrayList = ArrayList<FollowCrossRef>().apply { addAll(followCrossRefList) }
        insertArray(followArrayList)
    }

    @Transaction
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun bulkInsertFollowingsOfUser(userFollowing: UserEntity, followings: List<UserEntity>) {
        val followCrossRefList = followings.map { FollowCrossRef(userFollowing.userId, it.userId) }
        val followArrayList = ArrayList<FollowCrossRef>().apply { addAll(followCrossRefList) }
        insertArray(followArrayList)
    }

    @Transaction
    @Query("DELETE FROM github_follow_relation WHERE following_user = :userId")
    suspend fun deleteFollowersOfUser(userId: Long)

    @Transaction
    @Query("DELETE FROM github_follow_relation WHERE followed_user = :userId")
    suspend fun deleteFollowingsOfUser(userId: Long)
}