package com.sst.tutorial.githubfetcher.core.database.entities

import androidx.room.*

@Entity(primaryKeys = ["following_user", "followed_user"], tableName = "github_follow_relation")
data class FollowCrossRef(
    @ColumnInfo(name = "following_user") val followingUser: Long,
    @ColumnInfo(name = "followed_user") val followedUser: Long
) {
    data class FollowingOfUser(
        @Embedded val user: UserEntity,
        @Relation(
            parentColumn = "user_id",
            entityColumn = "user_id",
            associateBy = Junction(FollowCrossRef::class,
                    parentColumn = "following_user",
                    entityColumn = "followed_user")
        )
        val followingList: List<UserEntity>
    ) {
        constructor(): this(UserEntity(), ArrayList())
    }

    data class FollowersOfUser(
        @Embedded val user: UserEntity,
        @Relation(
            parentColumn = "user_id",
            entityColumn = "user_id",
            associateBy = Junction(FollowCrossRef::class,
                parentColumn = "followed_user",
                entityColumn = "following_user")
        )
        val followersList: List<UserEntity>
    ) {
        constructor(): this(UserEntity(), ArrayList())
    }
}