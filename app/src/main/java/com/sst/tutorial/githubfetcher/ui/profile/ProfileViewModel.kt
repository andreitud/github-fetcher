package com.sst.tutorial.githubfetcher.ui.profile

import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.*
import com.sst.tutorial.githubfetcher.core.database.GithubDatabase
import com.sst.tutorial.githubfetcher.core.model.UserProfile
import com.sst.tutorial.githubfetcher.core.repository.GithubRepository
import com.sst.tutorial.githubfetcher.core.service.GithubService
import kotlinx.coroutines.*
import java.lang.IllegalArgumentException

class ProfileViewModel(private val repository: GithubRepository, private val userLogin: String?): ViewModel() {
    val userProfile = MutableLiveData<UserProfile>()

    val visibleWhenNull = Transformations.map(userProfile) {
        if(null == userProfile.value) View.VISIBLE else View.GONE
    }
    val visibleWhenNotNull = Transformations.map(userProfile) {
        if(null != userProfile.value) View.VISIBLE else View.GONE
    }
    private val _isMyProfile = MutableLiveData<Boolean>(userLogin == null)
    val isMyProfile: LiveData<Boolean>
        get() = _isMyProfile

    private val job = Job()
    private val viewModeScope = CoroutineScope(Dispatchers.IO + job)

    init {
        Log.d("ProfileViewModel", "userLogin: $userLogin")
    }

    fun fetchProfile() {
        viewModeScope.launch {
            val profile = userLogin?.let { repository.getProfile(it) } ?: repository.getMyProfile()
            userProfile.postValue(profile)
        }
    }

    class Factory(private val context: Context, private val userLogin: String?): ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            @Suppress("UNCHECKED_CAST")
            if(modelClass.isAssignableFrom(ProfileViewModel::class.java)) {
                val database = GithubDatabase(context)
                val repository = GithubRepository(GithubService(), database)
                return ProfileViewModel(repository, userLogin) as T
            }
            throw IllegalArgumentException("The argument is not assignable from ProfileViewModel.")
        }

    }
}