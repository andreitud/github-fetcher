package com.sst.tutorial.githubfetcher.ui.login

import android.content.Context
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sst.tutorial.githubfetcher.core.database.GithubDatabase
import com.sst.tutorial.githubfetcher.core.repository.GithubRepository
import com.sst.tutorial.githubfetcher.core.service.GithubService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException

class LoginViewModel(private val repository: GithubRepository): ViewModel() {

    val editText = MutableLiveData<String>()
    val loginState = MutableLiveData<State>(State.AUTO_LOGIN_CHECK)

    val progressBarVisibility = Transformations.map(loginState) {
        if(it?.isChecking() == true) View.VISIBLE else View.INVISIBLE
    }
    val warningVisibility = Transformations.map(loginState) {
        if(it == State.LOGIN_FAILED) View.VISIBLE else View.GONE
    }
    val buttonEnabled = Transformations.map(loginState) {
        it != State.CHECKING_LOGIN
    }
    val hideInAutoLogin = Transformations.map(loginState) {
        if(it?.inAutoLogin() == true) View.GONE else View.VISIBLE
    }

    private val job = Job()
    private val viewModeScope = CoroutineScope(Dispatchers.IO + job)


    fun autoLogIn() {
        viewModeScope.launch {
            val alreadyLoggedIn = repository.alreadyLoggedIn()
            if(alreadyLoggedIn)
                loginState.postValue(State.AUTO_LOGIN_SUCCESSFUL)
            else
                loginState.postValue(State.WAITING_INPUT)
        }
    }

    fun onLoginClicked() {
        viewModeScope.launch {
            loginState.postValue(State.CHECKING_LOGIN)
            val username = editText.value?.toString() ?: ""
            val success = repository.login(username)

            if(success) {
                loginState.postValue(State.LOGIN_SUCCESSFUL)
            } else {
                loginState.postValue(State.LOGIN_FAILED)
            }
        }
    }

    /* TODO
    - hide drawer
    - fix logout
    - fix auto-log-in
    */

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }

    /** Auxiliary classes for LoginViewModel */

    enum class State {
        AUTO_LOGIN_CHECK,
        AUTO_LOGIN_SUCCESSFUL,
        WAITING_INPUT,
        CHECKING_LOGIN,
        LOGIN_FAILED,
        LOGIN_SUCCESSFUL;

        fun inAutoLogin(): Boolean {
            return this == AUTO_LOGIN_CHECK || this == AUTO_LOGIN_SUCCESSFUL
        }

        fun loginSuccess(): Boolean {
            return this == LOGIN_SUCCESSFUL || this == AUTO_LOGIN_SUCCESSFUL
        }

        fun isChecking(): Boolean {
            return this == AUTO_LOGIN_CHECK || this == CHECKING_LOGIN
        }
    }

    class Factory(val context: Context): ViewModelProvider.Factory {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if(modelClass.isAssignableFrom(LoginViewModel::class.java)) {
                val repository = GithubRepository(GithubService(), GithubDatabase(context))
                return LoginViewModel(repository) as T
            }
            throw IllegalArgumentException("Parameter modelClass passed to LoginViewModel.Factory " +
                    "is not assignable from LoginViewModel.")
        }

    }
}