package com.sst.tutorial.githubfetcher.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.sst.tutorial.githubfetcher.R
import com.sst.tutorial.githubfetcher.databinding.FragmentProfileBinding

class ProfileFragment: Fragment() {

    lateinit var binding: FragmentProfileBinding
    lateinit var viewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(inflater)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Create ViewModel
        val user = arguments?.getString("username")
        val factory = ProfileViewModel.Factory(requireContext(), user)
        viewModel = ViewModelProvider(this, factory).get(ProfileViewModel::class.java)
        binding.profileViewModel = viewModel

        // Go to repos button
        val goToRepos = View.OnClickListener {
            val args = bundleOf("username" to user)
            it?.findNavController()?.navigate(R.id.goToUserRepos, args)
        }
        binding.goToRepos = goToRepos

        viewModel.fetchProfile()
    }

}