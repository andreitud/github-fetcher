package com.sst.tutorial.githubfetcher.core.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.sst.tutorial.githubfetcher.core.database.entities.FollowCrossRef
import com.sst.tutorial.githubfetcher.core.database.entities.RepoEntity
import com.sst.tutorial.githubfetcher.core.database.entities.UserEntity
import com.sst.tutorial.githubfetcher.core.database.entities.UserInfoEntity

@Database(version = 1, entities =
    [UserEntity::class,
    UserInfoEntity::class,
    RepoEntity::class,
    FollowCrossRef::class])
abstract class GithubDatabase: RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun reposDao(): RepoDao
    abstract fun userInfoDao(): UserInfoDao
    abstract fun followDao(): FollowDao

    companion object {
        @Volatile
        private var instance: GithubDatabase? = null

        operator fun invoke(context: Context) = instance ?: synchronized(this) {
            buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context): GithubDatabase {
            return Room
                .databaseBuilder(context, GithubDatabase::class.java, "GithubFetcher.db")
                .build()
        }
    }
}