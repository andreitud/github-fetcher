package com.sst.tutorial.githubfetcher.ui.list.repo

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.sst.tutorial.githubfetcher.databinding.FragmentRepoListBinding

class RepoListFragment : Fragment() {
    private lateinit var viewModel: RepoListViewModel
    private lateinit var binding: FragmentRepoListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentRepoListBinding.inflate(inflater)
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Instantiating ViewModel
        val user = arguments?.getString("username")
        val viewModelFactory = RepoListViewModel.Factory(requireContext(), user)
        viewModel = ViewModelProvider(this, viewModelFactory).get(RepoListViewModel::class.java)
        binding.listViewModel = viewModel

        // Fetch repos list
        viewModel.fetchData()

        // Handling the repo list
        val adapter = RepoAdapter()
        adapter.submitList(viewModel.repoList.value)
        binding.repoListRecycler.adapter = adapter

        // Set observers
        viewModel.repoList.observe(viewLifecycleOwner) {
            if(it == null) {
                binding.repoListRecycler.visibility = View.GONE
                binding.repoListStatusText.visibility = View.VISIBLE
            } else {
                binding.repoListRecycler.visibility = View.VISIBLE
                binding.repoListStatusText.visibility = View.GONE
                adapter.submitList(it)
            }
        }
    }


}