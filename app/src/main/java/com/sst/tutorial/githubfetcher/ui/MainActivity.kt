package com.sst.tutorial.githubfetcher.ui
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.core.view.forEach
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.navigation.navArgs
import com.sst.tutorial.githubfetcher.NavGraphDirections
import com.sst.tutorial.githubfetcher.R
import com.sst.tutorial.githubfetcher.core.database.GithubDatabase
import com.sst.tutorial.githubfetcher.ui.list.user.UserListViewModel
import com.sst.tutorial.githubfetcher.ui.profile.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity(), MenuItem.OnMenuItemClickListener {

    private val navController by lazy {findNavController(R.id.navigationHostFragment)}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupMenu()
    }

    private fun setupMenu() {

        navigationDrawer.menu.forEach {
            it.setOnMenuItemClickListener(this)
        }

        val toggle = object : ActionBarDrawerToggle(this, drawerLayout,
            R.string.open_drawer, R.string.closer_drawer) {
            // Functions overrides expected
        }.apply {
            setHomeAsUpIndicator(R.drawable.ic_baseline_menu_24)
            isDrawerIndicatorEnabled = false
            syncState()
        }

        drawerLayout.addDrawerListener(toggle)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            if(destination.id == R.id.loginFragment) {
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            } else {
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            }

        }
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {

        // Desired behavior of drawer buttons is to reset the navigation
        // graph when clicking any of them AND to have the fragment showing
        // current user as home fragment
        drawerLayout.closeDrawer(GravityCompat.START)
        navController.popBackStack(R.id.myProfile, false)

        when(item?.itemId) {
            R.id.profileMenuItem -> {
                null
            }
            R.id.reposMenuItem -> {
                object : NavDirections {
                    override fun getActionId() = R.id.goToUserRepos
                    override fun getArguments() = bundleOf("username" to null)
                }
            }
            R.id.followersMenuItem -> {
                val type = UserListViewModel.ListType.FOLLOWERS
                object : NavDirections {
                    override fun getActionId() = R.id.gotToUserList
                    override fun getArguments() = bundleOf("listType" to type.value)
                }
            }
            R.id.followingMenuItem -> {
                val type = UserListViewModel.ListType.FOLLOWING
                object : NavDirections {
                    override fun getActionId() = R.id.gotToUserList
                    override fun getArguments() = bundleOf("listType" to type.value)
                }
            }
            R.id.logoutMenuItem -> {
                GlobalScope.launch {
                    GithubDatabase(applicationContext).userDao().logoutUser()
                }
                navController.popBackStack(R.id.myProfile, true)
                navController.navigate(R.id.gotToLogin)

                null
            }
            else -> null
        }?.let { action ->
            navController.navigate(action)
        }

        return item?.let { super.onOptionsItemSelected(it) } ?: false
    }

}