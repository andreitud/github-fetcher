package com.sst.tutorial.githubfetcher.core.database.entities

import androidx.room.Embedded
import androidx.room.Relation


data class ProfileEntity(
    @Embedded val userEntity: UserEntity,
    @Relation(
        parentColumn = "user_id",
        entityColumn = "user_info_id"
    )
    val userInfoEntity: UserInfoEntity?
) {
    constructor(): this(UserEntity(), UserInfoEntity())
}
