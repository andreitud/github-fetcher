package com.sst.tutorial.githubfetcher.ui.list.repo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sst.tutorial.githubfetcher.core.database.entities.RepoEntity
import com.sst.tutorial.githubfetcher.databinding.ListItemRepoBinding

class RepoAdapter: ListAdapter<RepoEntity, RepoAdapter.ViewHolder>(
    RepoDiffUtil
) {
    class ViewHolder private constructor(private val binding: ListItemRepoBinding)
        : RecyclerView.ViewHolder(binding.root) {

        fun bind(repo: RepoEntity) {
            binding.repo = repo
            binding.executePendingBindings()
        }

        companion object {
            operator fun invoke(parent: ViewGroup): ViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val binding = ListItemRepoBinding.inflate(inflater)
                return ViewHolder(
                    binding
                )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            parent
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let { holder.bind(it) }
    }

    object RepoDiffUtil: DiffUtil.ItemCallback<RepoEntity>() {
        override fun areItemsTheSame(oldItem: RepoEntity, newItem: RepoEntity): Boolean {
            return oldItem.repoId == newItem.repoId
        }

        override fun areContentsTheSame(oldItem: RepoEntity, newItem: RepoEntity): Boolean {
            return oldItem == newItem
        }

    }

}