package com.sst.tutorial.githubfetcher.ui.list.user

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sst.tutorial.githubfetcher.core.database.entities.UserEntity
import com.sst.tutorial.githubfetcher.databinding.ListItemUserBinding

class UserAdapter(private val listener: ItemClickListener):
    ListAdapter<UserEntity, UserAdapter.ViewHolder>(UserDiffUtil) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent, listener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let { holder.bind(it) }
    }

    /** AUXILIARY CLASSES */

    class ViewHolder private constructor(private val binding: ListItemUserBinding,
                                         private val listener: ItemClickListener)
        : RecyclerView.ViewHolder(binding.root) {

        fun bind(user: UserEntity) {
            binding.root.setOnClickListener {
                listener.onItemClick(it, user)
            }
            binding.user = user
            binding.executePendingBindings()
        }

        companion object {
            operator fun invoke(parent: ViewGroup, listener: ItemClickListener): ViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val binding = ListItemUserBinding.inflate(inflater)
                return ViewHolder(binding, listener)
            }
        }
    }

    object UserDiffUtil: DiffUtil.ItemCallback<UserEntity>() {
        override fun areItemsTheSame(oldItem: UserEntity, newItem: UserEntity): Boolean {
            return oldItem.userId == newItem.userId
        }

        override fun areContentsTheSame(oldItem: UserEntity, newItem: UserEntity): Boolean {
            return oldItem == newItem
        }

    }

    interface ItemClickListener {
        fun onItemClick(view: View?, user: UserEntity)
    }
}