package com.sst.tutorial.githubfetcher.core.service

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.sst.tutorial.githubfetcher.core.service.response.RepoResponse
import com.sst.tutorial.githubfetcher.core.service.response.UserProfileResponse
import com.sst.tutorial.githubfetcher.core.service.response.UserResponse
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface GithubService {

    @GET("users/{user}")
    suspend fun getUserProfile(@Path("user") user: String): UserProfileResponse

    @GET("users/{user}/repos")
    suspend fun getUserProjects(@Path("user") user: String): List<RepoResponse>

    @GET("users/{user}/{repo_name}")
    suspend fun getRepoDetails(@Path("user") user: String,
                       @Path("repo_name") repoName: String): RepoResponse

    @GET("users/{user}/following")
    suspend fun getFollowingOfUser(@Path("user") user: String): List<UserResponse>

    @GET("users/{user}/followers")
    suspend fun getFollowersOfUser(@Path("user") user: String): List<UserResponse>

    companion object {
        @Suppress("PRIVATE_PROPERTY")
        const val GITHUB_API_ADDRESS: String = "https://api.github.com/"

        @Volatile
        private var instance: GithubService? = null

        operator fun invoke(): GithubService {
            return instance ?: createService().also { instance = it }
        }

        private fun createService(): GithubService {
            val moshi = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()
            return Retrofit.Builder()
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .baseUrl(GITHUB_API_ADDRESS)
                .build()
                .create(GithubService::class.java)
        }
    }
}
