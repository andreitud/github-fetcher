package com.sst.tutorial.githubfetcher.core.database

import androidx.room.*

@Dao
interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(t: T): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertArray(ts: ArrayList<T>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(t: T): Int

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateArray(ts: ArrayList<T>): Int

    @Delete
    suspend fun delete(t: T)

    @Delete
    suspend fun deleteArray(ts: ArrayList<T>)

}