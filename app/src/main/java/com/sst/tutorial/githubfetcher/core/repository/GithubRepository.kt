package com.sst.tutorial.githubfetcher.core.repository

import com.sst.tutorial.githubfetcher.core.database.GithubDatabase
import com.sst.tutorial.githubfetcher.core.database.entities.RepoEntity
import com.sst.tutorial.githubfetcher.core.database.entities.UserEntity
import com.sst.tutorial.githubfetcher.core.model.UserProfile
import com.sst.tutorial.githubfetcher.core.service.GithubService
import com.sst.tutorial.githubfetcher.core.service.response.UserProfileResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GithubRepository private  constructor(private val githubService: GithubService,
                                            private val database: GithubDatabase) {

    /** Functions regarding the profile */

    suspend fun login(username: String): Boolean {
        return try {
            val userProfile = githubService.getUserProfile(username)
            insertProfile(userProfile)
            database.userDao().loginUser(username)
            true
        } catch (e: Throwable) {
            false
        }
    }

    suspend fun logout() {
        database.userDao().logoutUser()
    }

    suspend fun alreadyLoggedIn() = database.userDao().userLoggedIn() == 1

    suspend fun getMyProfile(): UserProfile {
        val profileEntity = database.userDao().getLoggedProfile()

        return UserProfile.fromProfileEntity(profileEntity)
    }

    suspend fun getProfile(login: String): UserProfile {
        val profileEntity = database.userDao().getProfile(login)

        return if (profileEntity == null
            || profileEntity.userEntity.userId == 0L
            || profileEntity.userInfoEntity == null
            || profileEntity.userInfoEntity.userInfoId == 0L
        ) {
            val profileResponse = githubService.getUserProfile(login)
            UserProfile.fromResponse(profileResponse)
        } else {
            UserProfile.fromProfileEntity(profileEntity)
        }
    }

    private suspend fun insertProfile(userProfileResponse: UserProfileResponse) {
        val userEntity = userProfileResponse.asUserEntity()
        val userInfoEntity = userProfileResponse.asUserInfoEntity()
        database.userDao().insert(userEntity)
        database.userInfoDao().insert(userInfoEntity)
    }

    private suspend fun fetchMyUpdatedProfile(): UserProfile {
        val loggedUser = database.userDao().getLoggedUser()
        val profileResponse = githubService.getUserProfile(loggedUser.login)
        withContext(Dispatchers.IO) {
            insertProfile(profileResponse)
        }
        return UserProfile.fromResponse(profileResponse)
    }

    /** Functions regarding the repos */

    suspend fun getUserRepos(userLogin: String?): ArrayList<RepoEntity> {
        var repoList = userLogin?.let { database.reposDao().getReposOfUser(userLogin) }
            ?: database.userDao().getLoggedUser().let{
                database.reposDao().getReposOfUser(it.login)
            }
        if(repoList.isEmpty()) {
            repoList = userLogin?.let { fetchUpdatedRepos(it) } ?: fetchMyUpdatedRepos()
        }

        return ArrayList<RepoEntity>().apply {
            addAll(repoList)
        }
    }

    private suspend fun fetchMyUpdatedRepos(): ArrayList<RepoEntity> {
        val loggedUser = database.userDao().getLoggedUser()
        return fetchUpdatedRepos(loggedUser.login)
    }

    private suspend fun fetchUpdatedRepos(userLogin: String): ArrayList<RepoEntity> {
        val reposResponseList = githubService.getUserProjects(userLogin)
        val reposEntityList = reposResponseList.map { it.dbRepo() }
        val reposArrayList = ArrayList<RepoEntity>().apply {addAll(reposEntityList)}

        database.reposDao().insertArray(reposArrayList)

        return reposArrayList
    }

    /** Functions regarding followers and followings of a user */

    suspend fun getMyFollowings(): List<UserEntity> {
        val loggedUser = database.userDao().getLoggedUser()
        val followings  = database.userDao().getFollowings(loggedUser.userId)

        if(followings.followingList.isEmpty()) {
            return fetchFollowingsOfUser(loggedUser.login)
        }
        return followings.followingList
    }

    suspend fun getMyFollowers(): List<UserEntity> {
        val loggedUser = database.userDao().getLoggedUser()
        val followings = database.userDao().getFollowers(loggedUser.userId)

        if(followings.followersList.isEmpty()) {
            return fetchFollowersOfUser(loggedUser.login)
        }
        return followings.followersList
    }

    private suspend fun fetchFollowingsOfUser(userLogin: String): List<UserEntity> {
        val currentUser = githubService.getUserProfile(userLogin).dbUser()
        val followings = githubService.getFollowingOfUser(userLogin).map { it.dbUser() }

        database.userDao().insertOrReplaceArray(ArrayList<UserEntity>().apply { addAll(followings) })

        database.followDao().bulkInsertFollowingsOfUser(currentUser, followings)

        return followings
    }

    private suspend fun fetchFollowersOfUser(userLogin: String): List<UserEntity> {
        val currentUser = githubService.getUserProfile(userLogin).dbUser()
        val followers = githubService.getFollowersOfUser(userLogin).map { it.dbUser() }

        database.userDao().insertOrReplaceArray(ArrayList<UserEntity>().apply { addAll(followers) })

        database.followDao().bulkInsertFollowersOfUser(currentUser, followers)

        return followers
    }

    companion object {
        private var instance: GithubRepository? = null

        operator fun invoke(service: GithubService, database: GithubDatabase) = instance ?:
                synchronized(this) {
                    GithubRepository(service, database).also { instance = it }
                }
    }
}