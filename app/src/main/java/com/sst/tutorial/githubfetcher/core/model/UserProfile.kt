package com.sst.tutorial.githubfetcher.core.model

import com.sst.tutorial.githubfetcher.core.database.entities.ProfileEntity
import com.sst.tutorial.githubfetcher.core.database.entities.UserEntity
import com.sst.tutorial.githubfetcher.core.database.entities.UserInfoEntity
import com.sst.tutorial.githubfetcher.core.service.response.UserProfileResponse

class UserProfile(
    val userId: Long,
    val login: String,
    val avatarUrl: String,
    val name: String?,
    val company: String?,
    val location: String?,
    val email: String?,
    val bio: String?,
    val public_repos: Int,
    val public_gists: Int,
    val followers: Int,
    val following: Int
) {

    companion object {
        fun fromResponse(response: UserProfileResponse) = UserProfile(
                userId = response.id,
                login = response.login,
                avatarUrl = response.avatarUrl,
                name = response.name,
                company = response.company,
                location = response.location,
                email = response.email,
                bio = response.bio,
                public_repos = response.publicRepos,
                public_gists = response.publicGists,
                followers = response.followers,
                following = response.following
            )

        fun fromProfileEntity(profileEntity: ProfileEntity) = fromUserEntities(
            profileEntity.userEntity,
            profileEntity.userInfoEntity ?: UserInfoEntity()
        )

        fun fromUserEntities(userEntity: UserEntity, infoEntityEntity: UserInfoEntity) = UserProfile(
                userId = userEntity.userId,
                login = userEntity.login,
                avatarUrl = userEntity.avatarUrl,
                name = infoEntityEntity.name,
                company = infoEntityEntity.company,
                location = infoEntityEntity.location,
                email = infoEntityEntity.email,
                bio = infoEntityEntity.bio,
                public_repos = infoEntityEntity.publicRepos,
                public_gists = infoEntityEntity.publicGists,
                followers = infoEntityEntity.followers,
                following = infoEntityEntity.following
            )
    }

}