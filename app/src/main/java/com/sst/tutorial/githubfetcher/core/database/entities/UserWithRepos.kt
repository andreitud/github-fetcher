package com.sst.tutorial.githubfetcher.core.database.entities

import androidx.room.Embedded
import androidx.room.Relation

data class UserWithRepos(
    @Embedded val user: UserEntity,
    @Relation(
        parentColumn = "user_id",
        entityColumn = "owner_id"
    )
    val repositories: List<RepoEntity>

    ) {
    constructor(): this(UserEntity(), ArrayList<RepoEntity>())
}