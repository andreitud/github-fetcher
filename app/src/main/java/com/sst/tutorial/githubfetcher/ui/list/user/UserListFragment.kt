package com.sst.tutorial.githubfetcher.ui.list.user

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.sst.tutorial.githubfetcher.R
import com.sst.tutorial.githubfetcher.core.database.entities.UserEntity
import com.sst.tutorial.githubfetcher.databinding.FragmentUserListBinding

class UserListFragment: Fragment(), UserAdapter.ItemClickListener {
    private lateinit var viewModel: UserListViewModel
    private lateinit var binding: FragmentUserListBinding
    private val args: UserListFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentUserListBinding.inflate(inflater)
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Instantiating ViewModel
        val factory = UserListViewModel.Factory(args.listType, requireContext())
        viewModel = ViewModelProvider(this, factory).get(UserListViewModel::class.java)
        binding.userListViewModel = viewModel

        // Handling user list
        val adapter = UserAdapter(this)
        adapter.submitList(viewModel.userList.value)
        binding.userListRecycler.adapter = adapter

        viewModel.userList.observe(viewLifecycleOwner) {
            if(it == null) {
                binding.userListStatusText.visibility = View.VISIBLE
                binding.userListRecycler.visibility = View.GONE
            }
            else {
                binding.userListStatusText.visibility = View.GONE
                binding.userListRecycler.visibility = View.VISIBLE
                adapter.submitList(it)
            }
        }
    }

    override fun onItemClick(view: View?, user: UserEntity) {
        val args = bundleOf("username" to user.login)
        Log.d("ProfileListener", "login: ${user.login} \t user: $user")
        view?.findNavController()?.navigate(R.id.goToUserProfile, args)
    }
}