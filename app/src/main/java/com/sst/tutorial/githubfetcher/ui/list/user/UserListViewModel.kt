package com.sst.tutorial.githubfetcher.ui.list.user

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sst.tutorial.githubfetcher.core.database.GithubDatabase
import com.sst.tutorial.githubfetcher.core.database.entities.UserEntity
import com.sst.tutorial.githubfetcher.core.repository.GithubRepository
import com.sst.tutorial.githubfetcher.core.service.GithubService
import kotlinx.coroutines.*

class UserListViewModel(val type: Int,
                        private val repository: GithubRepository): ViewModel() {

    val userList = MutableLiveData<List<UserEntity>>(null)
    val statusText = Transformations.map(userList) {
        if(it == null) "Loading..."
        else "Data is fetched."
    }

    private val job = Job()
    private val viewModeScope = CoroutineScope(Dispatchers.IO + job)

    init {
        fetchUsers()
    }

    private fun fetchUsers() {
        viewModeScope.launch {
            val list = when(type) {
                ListType.FOLLOWING.value ->
                    repository.getMyFollowings()
                ListType.FOLLOWERS.value ->
                    repository.getMyFollowers()
                else -> null
            }

            userList.postValue(list)
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }

    /** AUXILIARY CLASSES */

    enum class ListType(val value: Int) {
        FOLLOWING(1),
        FOLLOWERS(2),
        NONE(0);

        fun intToListType(intVal: Int): ListType {
            return when(intVal) {
                1 -> FOLLOWING
                2 -> FOLLOWERS
                else -> NONE
            }
        }
    }

    class Factory(private val type: Int, private val context: Context): ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            @Suppress("UNCHECKED_CAST")
            if(modelClass.isAssignableFrom(UserListViewModel::class.java)) {
                val database = GithubDatabase(context)
                val service = GithubService()
                val repository = GithubRepository(service, database)

                return UserListViewModel(type, repository) as T
            }
            throw IllegalArgumentException("The modelClass provided is unknown.")
        }
    }
}