package com.sst.tutorial.githubfetcher.core.database

import androidx.room.Dao
import androidx.room.Query
import com.sst.tutorial.githubfetcher.core.database.entities.UserInfoEntity

@Dao
interface UserInfoDao: BaseDao<UserInfoEntity> {
    @Query("SELECT * FROM github_user_info WHERE user_info_id = :userInfoId")
    suspend fun getUserInfo(userInfoId: Long): UserInfoEntity
}
