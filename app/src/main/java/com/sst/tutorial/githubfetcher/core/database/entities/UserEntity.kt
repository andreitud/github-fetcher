package com.sst.tutorial.githubfetcher.core.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "github_user")
data class UserEntity(
    @PrimaryKey @ColumnInfo(name = "user_id") val userId: Long = 0L,
    @ColumnInfo(name = "login") val login: String = "",
    @ColumnInfo(name = "avatar_url") val avatarUrl: String = "",
    @ColumnInfo(name = "html_url") val htmlUrl: String = "",
    @ColumnInfo(name = "repos_url") val reposUrl: String = "",
    @ColumnInfo(name = "logged_in") val loggedIn: Int = 0
)

