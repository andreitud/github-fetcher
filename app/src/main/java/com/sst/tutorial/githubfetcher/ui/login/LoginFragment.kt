package com.sst.tutorial.githubfetcher.ui.login

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.sst.tutorial.githubfetcher.R
import com.sst.tutorial.githubfetcher.databinding.FragmentLoginBinding
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment: Fragment(), View.OnClickListener {

    lateinit var binding: FragmentLoginBinding
    lateinit var viewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(inflater)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val factory = LoginViewModel.Factory(requireContext())
        viewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)

        setObservers()
        binding.loginViewModel = viewModel
        binding.loginButton.setOnClickListener(this)

        viewModel.autoLogIn()
    }

    private fun setObservers() {
        viewModel.editText.observe(viewLifecycleOwner) {
            if(viewModel.loginState.value == LoginViewModel.State.LOGIN_FAILED) {
                viewModel.loginState.value = LoginViewModel.State.WAITING_INPUT
            }
        }
        viewModel.loginState.observe(viewLifecycleOwner) {
            if(it?.loginSuccess() == true) {
                findNavController().navigate(R.id.goToMyProfile)
            }
        }
    }

    override fun onClick(v: View?) {
        if(v?.id == loginButton.id) {
            val imm = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(v.windowToken, 0)

            viewModel.onLoginClicked()
        }
    }
}