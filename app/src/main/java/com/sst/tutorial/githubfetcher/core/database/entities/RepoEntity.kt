package com.sst.tutorial.githubfetcher.core.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "github_repo")
data class RepoEntity(
    @PrimaryKey @ColumnInfo(name = "repo_id") val repoId: Long,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "full_name") val fullName: String,
    @ColumnInfo(name = "owner_id") val ownerId: Long,
    @ColumnInfo(name = "html_url") val htmlUrl: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "language") val language: String,
    @ColumnInfo(name = "default_branch") val defaultBranch: String
) {
    val languageText: String
        get() = "Language: ${if (language.isEmpty()) "-" else language}"
}