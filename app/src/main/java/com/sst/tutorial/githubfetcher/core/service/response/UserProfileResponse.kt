package com.sst.tutorial.githubfetcher.core.service.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.sst.tutorial.githubfetcher.core.database.entities.UserInfoEntity

@JsonClass(generateAdapter = true)
class UserProfileResponse(
    val name: String? = null,
    val company: String? = null,
    val location: String? = null,
    val email: String? = null,
    val bio: String? = null,
    @Json(name = "public_repos") val publicRepos: Int = 0,
    @Json(name = "public_gists") val publicGists: Int = 0,
    val followers: Int = 0,
    val following: Int = 0,
    @Json(name = "created_at") val createdAt: String = "",
    @Json(name = "updated_at") val updatedAt: String = "",

    // Superclass arguments
    @Json(name = "avatar_url") avatarUrl: String,
    @Json(name = "events_url") eventsUrl: String,
    @Json(name = "followers_url")  followersUrl: String,
    @Json(name = "following_url") followingUrl: String,
    @Json(name = "gists_url") gistsUrl: String,
    @Json(name = "gravatar_id") gravatarId: String,
    @Json(name = "html_url") htmlUrl: String,
    id: Long,
    login: String,
    @Json(name = "node_id") nodeId: String,
    @Json(name = "organizations_url") organizationsUrl: String,
    @Json(name = "received_events_url") receivedEventsUrl: String,
    @Json(name = "repos_url") reposUrl: String,
    @Json(name = "site_admin") siteAdmin: Boolean,
    @Json(name = "starred_url") starredUrl: String,
    @Json(name = "subscriptions_url") subscriptionsUrl: String,
    type: String,
    url: String

): UserResponse(
    avatarUrl = avatarUrl,
    eventsUrl = eventsUrl,
    followersUrl = followersUrl,
    followingUrl = followingUrl,
    gistsUrl = gistsUrl,
    gravatarId = gravatarId,
    htmlUrl = htmlUrl,
    id = id,
    login = login,
    nodeId = nodeId,
    organizationsUrl = organizationsUrl,
    receivedEventsUrl = receivedEventsUrl,
    reposUrl = reposUrl,
    siteAdmin = siteAdmin,
    starredUrl = starredUrl,
    subscriptionsUrl = subscriptionsUrl,
    type = type,
    url = url
) {
    fun asUserEntity() = dbUser()
    fun asUserInfoEntity() = UserInfoEntity(
        userInfoId = id,
        name = name,
        company = company,
        location = location,
        email = email,
        bio = bio,
        publicRepos = publicRepos,
        publicGists = publicGists,
        followers = followers,
        following = following,
        createdAt = createdAt,
        updatedAt = updatedAt
    )
}