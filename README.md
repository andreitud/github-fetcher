# Github fetcher

### Introduction

This project is a simple app where the use can see informations about it's github account. These include viewing its profile, listing its repos, check his followers and followings (while also being able to check their profiles and repos).

Getting started
---

### Project structure

The project has 2 main scripting packages: **core**  and **ui**. 

It's centered around making requests to the github api, namely `api.github.com`, to fetch the following:
- followers of a certain user - `/users/{user}/followers`
- followings of a certain user - `/users/{user}/following`
- extra details about an user - `/users/{user}`
- list of repositories of a certain user - `/users/{user}/repos`
- details about a repo - `/users/{user}/{repo_name}` 
  - will be used in future versions

It has a local database with the next structure: 
![](attachments/github_sqlite_entities.png)


### Screenshots


| Login page | Profile screen | Repo list |
|---|---|---|
| ![](attachments/login_page.png) |  ![](attachments/my_profile.png) | ![](attachments/repo_list.png) |

| Navigation drawer | Following/followers list | Check profile |
|---|---|---|
|![](attachments/nav_drawer.png) | ![](attachments/user_follow_list.png) | ![](attachments/check_profile.png) |

### Uncertainties regarding implementation

- choosing repository to be a singletone
- the entity and reponse object chosed
  - for instance, I think I should have ignored more fields in the reponse objects
- many to many relationship
  - I failed to implement a the `CrossRef` class, as advised for such scenarios, and I implemented methods in the database object (don't know if it's a good alternative or not)
- the viewmodels structures 
  - I used, for instance, a lot of transformations 


<center> <bold>Thanks! And I'm waiting for feedback :)</bold> <center> 



